# Proyecto de Enfermedad "Parte 2" (VACUNACIÓN)

_El proyecto consiste en la simulación de una enfermedad infecciosa para analizar el comportamiento dentro de una población
y a su vez controlar la enfermedad por medio de la generacion de vacunas, utilizando conceptos asociados a la programacion 
orientada a objetos_

# Explicacion 

Dentro de una comunidad, los ciudadanos se ven expuestos a una enfermedad infecciosa, la simulacion analiza el comportamiento
del virus y determina a las personas contagiadas, graves y los casos activos, asi como los contactos estrechos y recuperados, 
ademas en este proyecto se agregan nuevos atributos asi como las edades y el historial medico de los ciudadanos informando si
tienen una enfermedad de base y/o afeccion, lo que puede afectar las probabilidades del ciudadano a ser infectado por el virus
y desarrollar un estado grave.

En esta segunda parte se implemento la generacion de vacunas con el fin de controlar la enfermedad dentro de esta comunidad, se 
generaron 3 diferentes vacunas todas con garantias diversas e indices de efectividad distintos, y existiendo 2 vacunas de una sola dosis y una de dos dosis (segunda vacunacion realizada 5 dias(steps) despues de la primera inoculacion el dia 14 y el dia 20) cabe mencionar que la vacunacion solo se le realizo al 50 % de la poblacion.

al finalizar la simulacion para la cantidad total de la poblacion se considera solo a los ciudadanos vivos y que no esten considerados graves.

**_PRE-REQUISITOS_**
 
 para ejecutar este programa java se debe contar con:

- Sistema operativo Linux 
- Java 
- IDE para el lenguaje java 

**_PARA EJECUTAR LA SIMULACION (PARTE 2)_**

- Descargue todos los archivos del repositorio 
- Abra su editor de texto y dirijase al lugar donde están ubicados los archivos que acaba de descargar
- Espere algunos segundos para que se ejecute el programa
- Si quiere utilizar diferentes valores para la comunidad, los dias de recuperacion, el numero de habitantes o el numero 
  de conexion fisica se pueden modificar dentro de la clase Main


**_POO EN EL PROGRAMA_**


``En el codigo se implemento una interfaz, herencia y 2 modos  package y para la simulacion y otro para las condiciones de las vacunas ``

package Simulador;


public class Enfermedad {
	
	private double prob_infeccion;
    private int dias_recuperacion;

    public Enfermedad(double prob_infeccion, int dias_recuperacion){
        this.prob_infeccion = prob_infeccion;
        this.dias_recuperacion = dias_recuperacion;
    }
    
    public void setProb_infeccion(double prob_infeccion) {
    	
    	this.prob_infeccion = prob_infeccion;
    }
    
    public double getInfeccion() {
    	
    	return prob_infeccion;
    }


**_CREADO CON_:**

- Java 
- Eclipse IDE 2021-03
- Codigo estructurado en modo package


# Autoras

_Patricia Fuentes - (PatriciaFuentesG)_

_Paz Echeverría - (pazjecheverriaf)_




 





