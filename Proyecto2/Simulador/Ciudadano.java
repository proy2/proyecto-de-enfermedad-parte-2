package Simulador;
import Condiciones.*;

public class Ciudadano {
	
	private int id_;
    private int contador;
    private int edad;
    private int tipo_vacuna;
    private int cant_inyecciones = 0;
    private double prob_de_gravedad;
    private boolean recuperado;
    private boolean enfermo;
    private boolean contacto_fisico;
    private boolean muerto;
    private boolean estar_vacunado;
    private boolean enfermedad_base;
    private boolean enfermedad_afeccion;
    private boolean grave;
    private boolean inmune;
    private Enfermedad enfermedad;
    private Comunidad comunidad;
    private int dias_enfermo;

    Ciudadano(int edad, int id_, Comunidad comunidad, Enfermedad enfermedad){

        this.id_ = id_;
        this.edad = edad;
        this.comunidad = comunidad;
        this.enfermedad = enfermedad;
        this.recuperado = false;
        this.enfermo = false;
        this.contacto_fisico = false;
        this.enfermedad_base = false;
        this.enfermedad_afeccion = false;
        this.muerto = false ;
        this.estar_vacunado = false;
        this.grave = false;
        this.inmune = false;
    }

 
    public void setEnfermo(boolean enfermo){
    	
        this.enfermo = enfermo;
    }

    public boolean getEnfermo(){
    	
        return this.enfermo;
    }

    public int getId(){
    	
        return this.id_;
    }
    
    public int getEdad() {
    	
    	return this.edad;
    }
    
    public void setRecuperado(boolean recuperado) {
    	
    	this.recuperado = recuperado;
    	
    }
    
    public boolean getRecuperado() {
    	
    	return this.recuperado;
    }
    public void setEstar_vacunado(boolean estar_vacunado) {
    	
    	this.estar_vacunado = estar_vacunado;
    	
    }
    
    public boolean getEstar_vacunado() {
    	
    	return this.estar_vacunado;
    }
  public void setMuerte(boolean muerto) {
    	
    	this.muerto = muerto;
    	
    }
    
    public boolean getMuerto() {
    	
    	return this.muerto;
    } 
    public void setInmune(boolean inmune) {
    	
    	this.inmune = inmune;
    	
    }
    
    public boolean getInmune() {
    	
    	return this.inmune;
    }
    
   public void setGrave(boolean grave) {
    	
    	this.grave = grave;
    	
    }
    
    public boolean getGrave() {
    	
    	return this.grave;
    }
    
    
    public void setEnfermo_base(boolean enfermedad_base) {
    	
    	this.enfermedad_base = enfermedad_base ;
    	
    }
    
    public boolean getEnfermo_base() {
    	
    	return this.enfermedad_base;
    }
    
   public void setEnfermedad_afeccion(boolean enfermedad_afeccion) {
    	
    	this.enfermedad_afeccion  = enfermedad_afeccion;
    	
    }
    
    public boolean getEnfermedad_afeccion() {
    	
    	return this.enfermedad_afeccion;
    }

    
    public void setCant_inyecciones() {
    	
    	this.cant_inyecciones += 1;
    	
    }
    
    public int getCant_inyecciones() {
    	
    	return cant_inyecciones;

    }
   public void setTipo_vacuna(int tipo_vacuna) {
    	
    	this.tipo_vacuna = tipo_vacuna;
    	
    }
    
    public int getTipo_vacuna() {
    	
    	return tipo_vacuna;

    }
   public void setdias_enfermo() {
    	
    	this.dias_enfermo += 1;
    	
    }
    
    public int getdias_enfermo() {
    	
    	return dias_enfermo;

    }
    public double GetProb_gravedad() {
    	
    	return prob_de_gravedad;
    }
    public void SetProb_gravedad(int prob_de_gravedad) {
    	this.prob_de_gravedad = prob_de_gravedad;
    }
    public void Condicion_previa() {
    	//Este metodo sirve para asignarle una probabilidad de gravedad dependiendo de su enfermedad
    	
    	if(enfermedad_base == true ) {
    		
    		Enfermedad_de_base tipo = new Enfermedad_de_base();
    		prob_de_gravedad =prob_de_gravedad + tipo.Nombre_de_la_enfermedad();
    	}else if(enfermedad_afeccion == true) {
    		
    		Afeccion tipo = new Afeccion();
    		prob_de_gravedad =prob_de_gravedad + tipo.Nombre_de_la_enfermedad();
    		
    	}
    	
    }



}
