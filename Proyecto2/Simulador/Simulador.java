package Simulador;

public class Simulador {
	
	private int total_dias;
	private Comunidad comunidad;
	
	public Simulador(int total_dias, Comunidad comunidad){
		
		this.total_dias = total_dias;
		this.comunidad = comunidad;
	}
	
	public void pasar_de_los_dias() {
		
		for(int p=0; p<total_dias; p++) {
			System.out.println("El dia " + (p+1) + " hay:");
			
			if( p == 0) {
				comunidad.generarCiudadanos();
		        comunidad.infectadosIniciales(5);
		        comunidad.casos_activos();
		        comunidad.recuperarse();
		        comunidad.total_infectados();
		        comunidad.promedio_edadYpoblacion();
		        comunidad.cantidad_vacunas();
		        comunidad.enfermedades_previas();
		      
				
			}else if (p == 13) {
				
				comunidad.contacto_fisico();
		        comunidad.casos_activos();
		        comunidad.recuperarse();
		        comunidad.total_infectados();
		        comunidad.fallecer();
		        comunidad.extraer_casos_graves();
		        comunidad.verificar_estado();
		        comunidad.vacunar1y2();
		        comunidad.vacuna3();
		        comunidad.consecuencias_de_las_vacunas();
		        
			}else if (p== 19) {
				
				comunidad.contacto_fisico();
		        comunidad.casos_activos();
		        comunidad.recuperarse();
		        comunidad.total_infectados();
		        comunidad.fallecer();
		        comunidad.extraer_casos_graves();
		        comunidad.verificar_estado();
		        comunidad.vacuna3();
		        comunidad.consecuencias_de_las_vacunas();
				
			}else if( p == 30){
				
				 comunidad.contacto_fisico();
			     comunidad.verificar_estado();
			     comunidad.fallecer();
				 comunidad.casos_activos();
				 comunidad.recuperarse();
				 comunidad.total_infectados();
				 comunidad.extraer_casos_graves();
				 comunidad.promedio_edadYpoblacion();
				 comunidad.resumen_final();
				
			}else {
			   comunidad.contacto_fisico();
		       comunidad.verificar_estado();
		       comunidad.fallecer();
			   comunidad.casos_activos();
			   comunidad.recuperarse();
			   comunidad.total_infectados();
			   comunidad.extraer_casos_graves();

			}
		System.out.println("\n*****************************");
		}
	



}
}
