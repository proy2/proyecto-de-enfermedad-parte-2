package Simulador;

public class Enfermedad {
	
	private double prob_infeccion;
    private int dias_recuperacion;

    public Enfermedad(double prob_infeccion, int dias_recuperacion){
        this.prob_infeccion = prob_infeccion;
        this.dias_recuperacion = dias_recuperacion;
    }
    
    public void setProb_infeccion(double prob_infeccion) {
    	
    	this.prob_infeccion = prob_infeccion;
    }
    
    public double getInfeccion() {
    	
    	return prob_infeccion;
    }
    
    public void setDias_recuperacion(int dias_recuperacion) {
    	
    	this.dias_recuperacion = dias_recuperacion;
    }
    
    public int getDias_recuperacion() {
    	
    	return dias_recuperacion;
    }


}
