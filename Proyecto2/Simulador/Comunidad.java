package Simulador;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;


public class Comunidad {
	
	private int i ,total_infectados,dosis_entregadas = 0;;
	private int cant_vac1,cant_vac2,cant_vac3,cant_vacunas3;
	private double promedio_edad;
	private int muertos = 0;
	private int recuperados = 0;
	private int grave = 0;
	private int numero_habitantes;
    private int promedio_con_fisica;
    private int cant_vacunados_total;
    private ArrayList<Ciudadano> personas;
    private ArrayList<Ciudadano> cont_fisico;
    private Enfermedad enfermedad;
    private Vacuna1 vacuna1;
    private Vacuna2 vacuna2;
    private Vacuna3 vacuna3;
    private double probabilidad_con_fisica;
	private int cant_vacunas;
    Random rd = new Random();
	private double porcentaje;

    public Comunidad (int numero_habitantes, int promedio_con_fisica, double probabilidad_con_fisica, Enfermedad enfermedad, Vacuna1 vacuna1, Vacuna2 vacuna2, Vacuna3 vacuna3){

        this.numero_habitantes = numero_habitantes;
        this.promedio_con_fisica = promedio_con_fisica;
        this.probabilidad_con_fisica = probabilidad_con_fisica;
        this.enfermedad = enfermedad;
        this.vacuna1 = vacuna1;
        this.vacuna2 = vacuna2;
        this.vacuna3 = vacuna3;
        this.personas  = new ArrayList<Ciudadano>();
        this.cont_fisico = new ArrayList<Ciudadano>();    

       }
   
    public void generarCiudadanos(){

        // Se crean ciudadanos y se meten a la lista.
        for(int i=0; i<this.numero_habitantes; i++){
        	int e = (int)(Math.random()*100+1);
        	
            this.personas.add(new Ciudadano(e,i, this, this.enfermedad));
           
        }
    }

    public void infectadosIniciales(int iniciales){

        // Creacion de infectados iniciales
        Collections.shuffle(this.personas);

        for(int i=0; i<iniciales; i++){
            this.personas.get(i).setEnfermo(true);
            }
        }
    
    public void contacto_fisico() {
    	
    	this.cont_fisico.clear();
    	for (Ciudadano c: this.personas){
    		
            if (c.getEnfermo()== true&&(c.getInmune() == true) ) {
            	
            	for (int i = 0; i < promedio_con_fisica; i++) {
            		
            		int n = (int)(Math.random()*numero_habitantes);
            		this.cont_fisico.add(this.personas.get(n));
            		}
             }else if (c.getEnfermo() == true && c.getGrave() == false) {
            	 for (int i = 0; i < promedio_con_fisica; i++) {
             		
             		int n = (int)(Math.random()*numero_habitantes);
             		this.cont_fisico.add(this.personas.get(n));
             		}
             }else if (c.getEnfermo() == true) {
            	 for (int i = 0; i < promedio_con_fisica; i++) {
             		
             		int n = (int)(Math.random()*numero_habitantes);
             		this.cont_fisico.add(this.personas.get(n));
             		}
             }
             }
     }    	
     
   public void casos_activos() {
	   
	   i = 0;
	   for (Ciudadano c: this.personas){
		   
           if (c.getEnfermo()== true ) {
        	   i = i+1;
        	   
	          }
           
           } 
	   System.out.println("Total casos activos: " + i);
	   
   }
   
   public void total_infectados() {
       total_infectados = 0;	 
	   for (Ciudadano c: this.personas){
           if (c.getEnfermo()== true || c.getRecuperado() == true) {
        	  
        	   total_infectados = total_infectados + 1;        	   
           }
          }
	   System.out.println("Total casos infectados: " + total_infectados);
	   
	   
   }
   
   public void recuperarse() {
	   
	   for (Ciudadano c: this.personas){
		   if(c.getEnfermo() == true && c.getMuerto() == false) {
			   			  
			   if (c.getdias_enfermo() == enfermedad.getDias_recuperacion()) {
				   
				   c.setEnfermo(false);
				   c.setRecuperado(true);
				   recuperados = recuperados+1;
			   } else {
				   
				  c.setdias_enfermo();  
			   }   
			  }
		    }
	   System.out.println("Cantidad de personas recuperadas: "+recuperados);
	   }
	      
    
   public void verificar_estado() { 
    	

   	for (Ciudadano c: this.cont_fisico){   
   		int prob_gravedad = (int) c.GetProb_gravedad();
    	if ( c.getEnfermo() == false && c.getRecuperado() == false&& c.getInmune() == false){
    		if(this.rd.nextDouble() < this.probabilidad_con_fisica){
    			if(this.rd.nextDouble() < enfermedad.getInfeccion()){
    				c.setEnfermo(true);
    				prob_gravedad = prob_gravedad + 25;
    				c.SetProb_gravedad(prob_gravedad);
              }
    	}
      }
    }

 }
   
   public void cantidad_vacunas() {
		
		
	    cant_vacunas = (numero_habitantes*60)/100;
		
		System.out.println("Cantidad de vacunas:"+cant_vacunas);
		//Ahora se reparte el total de vacunas segun  el tipo de vacuna que es
		
		cant_vac1 =   cant_vacunas-(cant_vacunas/2);
		vacuna1.SetCant_Vacuna(cant_vac1);
		vacuna1.GetCant_vacuna();
		cant_vac2 = (cant_vac1)/2;
		vacuna2.SetCant_Vacuna(cant_vac2);
		vacuna2.GetCant_vacuna();
		cant_vac3 = cant_vac2;
		cant_vacunas3 = cant_vac3;
		vacuna3.SetCant_Vacuna(cant_vac3);
		vacuna3.GetCant_vacuna();
		
}
  public void promedio_edadYpoblacion() {
	  
	  double m = 0;
	  double suma_edades = 0;
	  promedio_edad = 0;
	  for (Ciudadano c: this.personas){
		  if(c.getMuerto() == false ) {
			  
			 m = m + 1;
			 suma_edades = suma_edades + c.getEdad();  
		  }
	   }
	  promedio_edad = suma_edades/m;
	  System.out.println("Promedio de las edades :" + promedio_edad);
	 
	  }
  
  public void enfermedades_previas() {
	  
	  int p  = 0;
	  int m = 0;
	  porcentaje = (numero_habitantes*50)/100;
	  for (Ciudadano c: this.personas){
		  int i = (int)(Math.random()*3+1);
		  if (p == porcentaje){
			  break;
		  }
		  else if(i == 1) {
			  p = p+1;
			  c.setEnfermo_base(true);
			  c.Condicion_previa();
			 
		  }else if (i == 2) {
			  p = p+1;
			  c.setEnfermedad_afeccion(true);
			  c.Condicion_previa();
			  
			  
		  }else if (i == 3) {
			  p = p+1;
			  c.setEnfermedad_afeccion(true);
			  c.setEnfermo_base(true);
			  c.Condicion_previa();
			 
		  }
		
	   }
	  for(Ciudadano c: this.personas) {
		  if(c.getEnfermedad_afeccion() == true || c.getEnfermo_base() == true) {
	    	 m=m+1;
	     }
	  }
	  System.out.println("Cantidad de enfermos de base y/o afeccion: "+ m);
	  System.out.println("Cantidad de poblacion: "+numero_habitantes);
	 }
  
  public void extraer_casos_graves() {
	  

	   
	  for (Ciudadano c: this.personas){
		 
		   if((c.GetProb_gravedad() >= 35 && c.GetProb_gravedad() <= 78)&&c.getGrave()== false &&c.getEnfermo() == true){ 
			   c.setGrave(true);
			   grave = grave +1;
			 
			   
			  }
		  }
	System.out.println("Totad de personas graves: "+grave);
	  
	   
  }
  public void fallecer() {

	  for( Ciudadano c: this.personas) {
		  if (c.getEnfermo()== true && c.getMuerto() == false) {
			  int i = (int)(Math.random()*1000+1);
			  if (c.GetProb_gravedad() >= i) {
				  c.setMuerte(true);
				  muertos = muertos+1;
			  }
		  }
		  
	  }
	
	  
  }
  public void vacunar1y2() {
	  //  Con este metodo se vacunan  los tipos de vacuna 1 y 2 , los cuales solo requieren una dosis
	  
	  int cant_vacunados = 0 ;
	  
	  for (Ciudadano c: this.personas) {
		  cant_vacunados = 0;
		  if((vacuna1.GetCant_vacuna() == 0)&&(vacuna2.GetCant_vacuna() == 0)) {
			 break;
		  }
		
		  for (Ciudadano p: this.personas) {
			  if ( p.getEstar_vacunado() == true ) {
				  cant_vacunados = cant_vacunados +1;
			  }
		  }
		  if (cant_vacunados >= (numero_habitantes*50)/100){
			  break;
			  
		  }
		  else if(c.getTipo_vacuna() == 3) {
			  break;
		  
	      }else if ( vacuna1.GetCant_vacuna() > 0) {
			  if((((c.getEnfermo_base() == true||c.getEnfermedad_afeccion() == true)&& (c.getMuerto() == false)) && (c.getEdad() >= 75))&&c.getEstar_vacunado() == false) {
				  //este if sirve para priorizar a la parte de la poblacion que es mayor o igual a 75 a�os y que este enfermo de base o de afeccion 
			  
			  c.setTipo_vacuna(1);
			  c.setEstar_vacunado(true);
			  cant_vac1 = cant_vac1-1;
			  vacuna1.SetCant_Vacuna(cant_vac1);
			  }else if (((c.getEnfermo_base() == true||c.getEnfermedad_afeccion() == true)&& c.getMuerto() == false)&& c.getEstar_vacunado() == false) {
				  //En este se continua con la parte de la poblacion menor a 75 que tenga una enfermedad de base o de afeccion
				  c.setTipo_vacuna(1);
				  c.setEstar_vacunado(true);
				  cant_vac1 = cant_vac1-1;
				  vacuna1.SetCant_Vacuna(cant_vac1);
			  }else if (c.getMuerto() == false && c.getEstar_vacunado() == false) {
		        //en este se es resto de la poblacion 	
				  
				  c.setTipo_vacuna(1);
				  c.setEstar_vacunado(true);
				  cant_vac1 = cant_vac1-1;
				  vacuna1.SetCant_Vacuna(cant_vac1);
			  }
		   }else if (vacuna2.GetCant_vacuna() > 0) {
			  if((((c.getEnfermo_base() == true||c.getEnfermedad_afeccion() == true)&& (c.getMuerto() == false)) && (c.getEdad() >= 75))&& c.getEstar_vacunado() == false) {
				 
				 c.setTipo_vacuna(2);
				 c.setEstar_vacunado(true);
				 cant_vac2 = cant_vac2-1;
				 vacuna2.SetCant_Vacuna(cant_vac2);
			 }else if(((c.getEnfermo_base() == true||c.getEnfermedad_afeccion() == true)&& c.getMuerto() == false)&& c.getEstar_vacunado() == false) {
			   
				c.setTipo_vacuna(2);
			    c.setEstar_vacunado(true);
			    cant_vac2 = cant_vac2-1;
			    vacuna2.SetCant_Vacuna(cant_vac2);
			  }else if(c.getMuerto() == false && c.getEstar_vacunado() == false) {
				  c.setTipo_vacuna(2);
				  c.setEstar_vacunado(true);
				  cant_vac2 = cant_vac2-1;
				  vacuna2.SetCant_Vacuna(cant_vac2);
			  } 
		   }
	  } 
	
	  
  }
  public void vacuna3() {
	  //se escribe apartado de las otras vacunas ya que necesita ser entregada en dos dosis
	  int cant_vacunados = 0;

	  
	  for (Ciudadano c: this.personas) {
		 cant_vacunados = 0;
	
		  for (Ciudadano p: this.personas) {
			 if ( p.getEstar_vacunado() == true ) {
				  cant_vacunados = cant_vacunados +1;
			   }
		    }
		   if (cant_vacunados >= (numero_habitantes*50)/100){
			
			  break;
			  
			  
		  }else if (vacuna3.GetCant_vacuna() !=(cant_vacunas3/2) ||(vacuna3.GetCant_vacuna() ==(cant_vacunas3/2)&& dosis_entregadas == 1)) {
			if ( vacuna3.GetCant_vacuna()/2 > 0) {
			  if((((c.getEnfermo_base() == true||c.getEnfermedad_afeccion() == true)&& (c.getMuerto() == false)) && (c.getEdad() >= 75))&& c.getEstar_vacunado() == false) {
			  
			  c.setTipo_vacuna(3);
			  c.setCant_inyecciones();
			  if (c.getCant_inyecciones() >= 2) {
				  c.setEstar_vacunado(true);
			  }
			  cant_vac3 = cant_vac3-1;
			  vacuna3.SetCant_Vacuna(cant_vac3);
			  
		    }else if (((c.getEnfermo_base() == true||c.getEnfermedad_afeccion() == true)&& c.getMuerto() == false)&& c.getEstar_vacunado() == false) {
				  
				  c.setTipo_vacuna(3);
				  c.setCant_inyecciones();
				  if (c.getCant_inyecciones() >= 2) {
					  c.setEstar_vacunado(true);
				  }
				
				  cant_vac3 = cant_vac3-1;
				  vacuna3.SetCant_Vacuna(cant_vac3);
				 
			 }else if (c.getMuerto() == false && c.getEstar_vacunado() == false) {
				  c.setTipo_vacuna(3);
				  c.setCant_inyecciones();
				  if (c.getCant_inyecciones() >= 2) {
					  c.setEstar_vacunado(true);
				  }
				  cant_vac3 = cant_vac3-1;
				  vacuna3.SetCant_Vacuna(cant_vac3);
				 
			    }
			  }
			}else if(vacuna3.GetCant_vacuna() ==(cant_vacunas3/2)&&dosis_entregadas != 1) {
				  dosis_entregadas = 1 ;
				  break;
			  
			  }
    }
	 
 }
  
  public void consecuencias_de_las_vacunas() { 
	  int prob_gravedad;
	  for(Ciudadano c: this.personas) {
		  if (c.getTipo_vacuna() == 1) {
			  prob_gravedad = ((int) c.GetProb_gravedad())-25;
			  c.SetProb_gravedad(prob_gravedad);
			    
		  }else if (c.GetProb_gravedad() == 2) {
			  prob_gravedad = ((int) c.GetProb_gravedad())-30;
			  c.SetProb_gravedad(prob_gravedad);
		  }else if(c.getTipo_vacuna() == 3&&c.getEstar_vacunado()== true) {
			c.setInmune(true); 
		
			  
		  }
	  }
  }
  public void resumen_final() {
	  
	  int total_poblacion = numero_habitantes;
	  int recuperado = 0;
	  int cant_recuperados = 0;
	  
	  for(Ciudadano c: this.personas) {

		 if(c.getMuerto() == true || c.getGrave() == true||(c.getMuerto()== true && c.getGrave() ==true)) {
			 total_poblacion = total_poblacion-1;
		 }
		 if (c.getMuerto() == true&&(c.getEnfermedad_afeccion()==true||c.getEnfermo_base()==true)) {
			 porcentaje -= 1;		 
		 }
		if((c.getInmune() == true)||c.getRecuperado()== true) {
			 recuperado = recuperado+1;
		
		  } 
	   if ( c.getEstar_vacunado() == true && c.getMuerto()==false) {
		   cant_vacunados_total = cant_vacunados_total+1;
				
		 }
			
	
    
	  
	  }
	  System.out.println("Cantidad de enfermos de base y/o afeccion: "+porcentaje);
	  System.out.println("Cantindad de inoculados: "+cant_vacunados_total);
	  System.out.println("Total de Inmunes o recuperados: "+recuperado);
	  System.out.println("Total de fallecidos: "+ muertos);
	  System.out.println("Cantidad de poblacion: "+total_poblacion);
    }
  
}
